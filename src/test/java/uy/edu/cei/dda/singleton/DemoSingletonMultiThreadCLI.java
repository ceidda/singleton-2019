package uy.edu.cei.dda.singleton;

import java.util.function.Function;

public class DemoSingletonMultiThreadCLI {

	public static void main(String... args) {

		Function<String, String> func = (value) -> {
			DemoSingletonMultiThread s = DemoSingletonMultiThread.getInstance();
			System.out.println("despues del get instance" + Thread.currentThread().getName());
			System.out.println(s);
			System.out.println("antes de hello world " + Thread.currentThread().getName());
			s.helloWorld();
			System.out.println("despues de hello world " + Thread.currentThread().getName());
			return "";
		};

		Runnable r1 = new Runnable() {
			@Override
			public void run() {
				func.apply("");
			}
		};

		Thread h1 = new Thread(r1, "h1");

		Thread h2 = new Thread(() -> {
			func.apply("");
		}, "h2");

		h1.start();
		h2.start();

	}
}
