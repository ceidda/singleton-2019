package uy.edu.cei.dda.singleton;

import static org.junit.Assert.*;

import org.junit.Test;

public class DemoSingletonTest {

	@Test
	public void test() {
		DemoSingleton demoSingleton1 = DemoSingleton.getInstance();
		DemoSingleton demoSingleton2 = DemoSingleton.getInstance();
		assertNotNull(demoSingleton1);
		assertNotNull(demoSingleton2);
		assertEquals(demoSingleton1, demoSingleton2);
	}

}
