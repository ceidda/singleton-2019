package uy.edu.cei.dda.singleton;

import static org.junit.Assert.*;

import org.junit.Test;

public class DemoSingletonStaticTest {

	@Test
	public void test() {
		DemoSingletonStatic demoSingletonStatic1 = DemoSingletonStatic.getInstance();
		DemoSingletonStatic demoSingletonStatic2 = DemoSingletonStatic.getInstance();

		assertEquals(demoSingletonStatic1, demoSingletonStatic2);
	}

	private Object DemoSingletonStatic() {
		// TODO Auto-generated method stub
		return null;
	}

}
