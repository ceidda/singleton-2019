package uy.edu.cei.dda.singleton;

import static org.junit.Assert.*;

import org.junit.Test;

public class DemoSingletonAttributeTest {

	@Test
	public void test() {
		DemoSingletonAttribute demoSingletonAttribute1 = DemoSingletonAttribute.getInstance();
		DemoSingletonAttribute demoSingletonAttribute2 = DemoSingletonAttribute.getInstance();
		
		assertEquals(demoSingletonAttribute1, demoSingletonAttribute2);
	}

}
