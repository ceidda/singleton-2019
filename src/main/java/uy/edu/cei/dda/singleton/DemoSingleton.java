package uy.edu.cei.dda.singleton;

public class DemoSingleton {

	public static final String message = "";
	
	private static DemoSingleton instance = null;

	public static DemoSingleton getInstance() {
		if (instance == null) {
			instance = new DemoSingleton();
		}
		return instance;
	}

	private DemoSingleton() {

	}

	public void helloWorld() {

	}
}
