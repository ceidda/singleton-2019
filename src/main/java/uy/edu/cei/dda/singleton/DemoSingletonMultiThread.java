package uy.edu.cei.dda.singleton;

public class DemoSingletonMultiThread {

	public static final String message = "";

	private static DemoSingletonMultiThread instance = null;
	
	private Object lock = new Object();

	public static synchronized DemoSingletonMultiThread getInstance() {
		if (instance == null) {
			instance = new DemoSingletonMultiThread();
		}
		return instance;
	}

	private DemoSingletonMultiThread() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void helloWorld() {
		synchronized (lock) {			
			System.out.println("hello world");
		}
	}
}
