package uy.edu.cei.dda.singleton;

public class DemoSingletonStatic {

	private static DemoSingletonStatic instance = null;
	
	static {
		instance = new DemoSingletonStatic();
	}
	
	public static DemoSingletonStatic getInstance() {
		return instance;
	}
	
	private DemoSingletonStatic() {
		
	}

}
