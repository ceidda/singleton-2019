package uy.edu.cei.dda.singleton;

public class DemoSingletonAttribute {

	private static DemoSingletonAttribute instance = new DemoSingletonAttribute();
	
	public static DemoSingletonAttribute getInstance() {
		return instance;
	}

	private DemoSingletonAttribute() {
		
	}
}
